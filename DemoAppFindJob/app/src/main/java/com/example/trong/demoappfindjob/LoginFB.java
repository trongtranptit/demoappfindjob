package com.example.trong.demoappfindjob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginFB extends AppCompatActivity {
    ProfilePictureView profilePictureView;
    private LoginButton btn_loginfb;
    private CallbackManager callbackManager;
    private String user_email, user_id, user_gender, user_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_login_fb);

        callbackManager = CallbackManager.Factory.create();
        addControls();

        btn_loginfb.setReadPermissions(Arrays.asList("public_profile","email","user_birthday","user_friends"));
        setLogin_fb();
    }

    private void setLogin_fb() {
        btn_loginfb.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // btn_loginfb.setVisibility(View.INVISIBLE);
                GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("JSON", response.getJSONObject().toString());
                        try {
                            user_gender = object.getString("gender");
                           // user_id = object.getString("id");
                            user_name = object.getString("name");
                            Log.d("user_info",user_id+" "+user_name+" "+user_gender);
                            profilePictureView.setProfileId(object.getString("id"));
                            Intent t = new Intent(LoginFB.this, MainActivity.class);
                            Bundle b = new Bundle();
                            b.putString("type","fb");
                            b.putString("object",object.toString());
                            t.putExtra("bundle",b);
                            startActivity(t);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    private void addControls() {
        btn_loginfb = (LoginButton) findViewById(R.id.btn_loginfb);
        profilePictureView = (ProfilePictureView) findViewById(R.id.iv_avatarfb);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

}
