package com.example.trong.demoappfindjob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    String object;
    String tokens[];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        processUserData();

    }

    private void processUserData() {
        Bundle b = getIntent().getBundleExtra("bundle");
        String type = b.getString("type");
        object = b.getString("object");
        if(type.equals("fb")){
            processFB();
        }
        else processGG();
    }

    public void processFB(){
        Log.e("object fb",object);
        tokens = object.split("[,{}:]");
        for(String i:tokens){
            Log.e("tokens fb",i);
        }
    }

    public  void processGG(){
        Log.e("object gg",object);
        tokens = object.split(" ");
        for(String i:tokens){
            Log.e("tokens gg",i);
        }
    }


}
